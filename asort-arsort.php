<?php
$arrTinggi=array("Bima"=>178, "Ara"=>165, "Indah"=>150, "Wahyu"=>182);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

asort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah diurutkan dengan asort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

arsort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah diurutkan dengan arsort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";
?>