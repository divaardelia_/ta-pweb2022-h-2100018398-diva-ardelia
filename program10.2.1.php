<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Konversi nilai</title>
</head>
<body>
	<form action="" method="GET" name="input">
		Masukkan nilai Anda : <input type="text" name="nilai"><br><br>
		<input type="submit" name="Input" value="Konversi Nilai"><br><br>
	</form>
</body>
</html>


<?php  
	if(isset($_GET['Input'])){
		$nilai=$_GET['nilai'];

		if($nilai >= 0.00 && $nilai <= 39.99){
			echo "nilai $nilai, nilai huruf E";
		}
		else if ($nilai >= 40.00 && $nilai <= 43.74) {
			echo "nilai $nilai, nilai huruf D";
		}
		else if ($nilai >= 43.75 && $nilai <= 51.24) {
			echo "nilai $nilai, nilai huruf D+";
		}
		else if ($nilai >= 51.25 && $nilai <= 54.99) {
			echo "nilai $nilai, nilai huruf C-";
		}
		else if ($nilai >= 55.00 && $nilai <= 57.49) {
			echo "nilai $nilai, nilai huruf C";
		}
		else if ($nilai >= 57.50 && $nilai <= 62.49) {
			echo "nilai $nilai, nilai huruf C+";
		}
		else if ($nilai >= 62.50 && $nilai <= 64.99) {
			echo "nilai $nilai, nilai huruf B-";
		}
		else if ($nilai >= 65.00 && $nilai <= 68.74) {
			echo "nilai $nilai, nilai huruf B";
		}
		else if ($nilai >= 68.75 && $nilai <= 76.24) {
			echo "nilai $nilai, nilai huruf B+";
		}
		else if ($nilai >= 76.25 && $nilai <= 79.99) {
			echo "nilai $nilai, nilai huruf A-";
		}
		else if ($nilai >= 80.00 && $nilai <= 100.00) {
			echo "nilai $nilai, nilai huruf A";
		}
		else{
			echo "Nilai yang Anda masukkan tidak memenuhi predikat huruf";
		}
	}
?>