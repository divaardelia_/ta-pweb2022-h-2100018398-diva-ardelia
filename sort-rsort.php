<?php
$arrTinggi=array("Bima"=>178, "Ara"=>165, "Indah"=>150, "Wahyu"=>182);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

sort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";

rsort($arrTinggi);
reset($arrTinggi);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrTinggi);
echo "</pre>";
?>